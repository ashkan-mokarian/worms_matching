#include "graph_matching/graph_matching.h"
#include "visitors/standard_visitor.hxx"
#include "solver.hxx"

#include <string>
#include <vector>

using namespace LPMP;

std::vector<std::string> options =
        {
                "",
                "--roundingReparametrization", "anisotropic:0.5",
                "--tightenIteration", "500",
                "--tightenInterval", "50",
                "--tightenReparametrization", "uniform:0.5",
                "--tightenConstraintsMax", "150",
                "--maxIter", "5000",
                "--tighten",
                "-v", "2"
        };


int pairwise_matching(const std::string& input_file_name, std::string output_file_name = "") {
    if (output_file_name == "") {
        output_file_name = input_file_name + ".sol";
    }
    options.push_back("-o");
    options.push_back(output_file_name);

    ProblemConstructorRoundingSolver<Solver<LP<FMC_MP_T<PairwiseConstruction::Left>>,StandardTighteningVisitor>> solver(options);
    auto input = TorresaniEtAlInput::parse_file(input_file_name);
    solver.template GetProblemConstructor<0>().construct(input);
    return solver.Solve();
}

int main() {
    const std::string file_name = "/home/ashkan/workspace/dataset/example/wormEval-worst-amira-gaps/elt3L1_0504073-to-egl5L1_0606074.surf-18-09-24-1414.dd";
    std::cout << "Hello, World!" << std::endl;
    return pairwise_matching(file_name);
}